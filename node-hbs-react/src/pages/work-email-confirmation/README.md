# README #

This section show some code parts of the Borrower TodoList project with LC


### SETUP ###

* At the moment is not possible to run code independently.

### This repository contains: ###

* Parts of Code of some projects made with: 
	* React
	* Node
	* LESS
* Testing with
	* Jest
	* chai
	* sinon

* Contains:
	* Unit Test	

### Pages used: ###
* Work Email confirmation page
![Alt text](readme-files/work-email-confirmation.gif?raw=true "Work Email Confirmation page")

### Next Steps ###

### Version Tracking ###
* Version 0.1
* 2018

### More information? ###

* Jaime Hernandez cruz
* jhdezcruz@gmail.com
