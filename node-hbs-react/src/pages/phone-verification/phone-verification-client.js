/* eslint-disable import/no-unresolved */
import 'base-layout';
import React from 'react';
import { render } from 'react-dom';
import PhoneVerification from './PhoneVerification';

render(<PhoneVerification />, document.getElementById('phone-verification'));
